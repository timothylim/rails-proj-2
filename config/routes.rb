Rails.application.routes.draw do
  root to: 'home#index'
  devise_for :users
  resources :users
  resources :outfits, only: [:create]
  patch 'outfits/capture', to:'outfits#capture', as: 'capture'
  patch 'outfits/damage', to:'outfits#damage', as: 'damage'
  get 'outfits/new', to:'outfits#new', as: 'new'
  post '/rate' => 'rater#create', :as => 'rate'
  devise_for :users
  devise_for :models
end
